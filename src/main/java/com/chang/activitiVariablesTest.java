package com.chang;

import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class activitiVariablesTest {

    @Test
    public void deployment(){

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        RepositoryService repositoryService = processEngine.getRepositoryService();

        Deployment deploy = repositoryService.createDeployment()
                .name("出差申请流程Variables")
                .addClasspathResource("bpmn/evection-uel-global.bpmn")
                .deploy();

        System.out.println("流程部署名字 :"+deploy.getName());
        System.out.println("流程部署ID  :"+deploy.getId());
    }

    @Test
    public void startAssigneeUel(){

        // 获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        //获取RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();

        //
        Evection evection = new Evection();
        evection.setNum(5d);
        evection.setId(2);


        // 设定assignee 并启动流程
        Map<String, Object> param = new HashMap<>();
        param.put("evection",evection);
        param.put("assignee0","xiaozhang");
        param.put("assignee1","xiaowang");
        param.put("assignee2","xiaoli");
        param.put("assignee3","xiaoliu");
        runtimeService.startProcessInstanceByKey("evection-uel-global",param);
    }


    @Test
    public void personal(){

        // 获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        //获取taskService
        TaskService taskService = processEngine.getTaskService();

        Task task = taskService.createTaskQuery()
                .taskAssignee("xiaoliu")
                .processDefinitionKey("evection-uel-global")
//                .deploymentId("95001")
                .singleResult();

        String processInstanceId = task.getId();

        taskService.complete(processInstanceId);

    }
}
