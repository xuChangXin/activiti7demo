package com.chang;

import java.io.Serializable;

public class Evection implements Serializable {
    private Integer id;
    private Double num;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getNum() {
        return num;
    }

    public void setNum(Double num) {
        this.num = num;
    }
}
