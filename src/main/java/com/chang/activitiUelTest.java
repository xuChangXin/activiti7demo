package com.chang;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.repository.Deployment;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class activitiUelTest {

    @Test
    public void deployment(){

        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        RepositoryService repositoryService = processEngine.getRepositoryService();

        Deployment deploy = repositoryService.createDeployment()
                .name("出差申请流程uel")
                .addClasspathResource("bpmn/evection-uel.bpmn")
                .deploy();

        System.out.println("流程部署名字"+deploy.getName());
        System.out.println("流程部署ID"+deploy.getId());
    }

    @Test
    public void startAssigneeUel(){

        // 获取流程引擎
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        //获取RuntimeService
        RuntimeService runtimeService = processEngine.getRuntimeService();


        // 设定assignee 并启动流程
        Map<String, Object> param = new HashMap<>();
        param.put("assignee0","xiaozhang");
        param.put("assignee1","xiaowang");
        param.put("assignee2","xiaoli");
        param.put("assignee3","xiaoliu");
        runtimeService.startProcessInstanceByKey("evection-uel",param);
    }


}
