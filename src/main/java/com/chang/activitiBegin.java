package com.chang;

import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.util.List;

public class activitiBegin {

    @Test
    public void testCreateTable(){
        //使用classpath下的activiti.cfg.xml中的配置创建processEngine
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = defaultProcessEngine.getRepositoryService();
        Deployment deployment = repositoryService.createDeployment()
                .name("账号申请流程")
                .addClasspathResource("bpmn/sign.bpmn")
                .addClasspathResource("bpmn/sign.png")
                .deploy();

        System.out.println("defaultProcessEngine = " + defaultProcessEngine);

//        InputStream inputStream =  this.getClass().getClassLoader().getResourceAsStream("activitiConfig.xml");
//
//        ProcessEngineConfiguration processEngineConfigurationConfiguration = ProcessEngineConfiguration.createProcessEngineConfigurationFromInputStream(inputStream, "processEngineConfigurationConfiguration");
//        ProcessEngine processEngine = processEngineConfigurationConfiguration.buildProcessEngine();
//        RepositoryService repositoryService1 = processEngine.getRepositoryService();
//        Deployment deploy = repositoryService1.createDeployment()
//                .name("申请账号流程 自定义")
//                .addClasspathResource("bpmn/sign.bpmn")
//                .addClasspathResource("bpmn/sign.png")
//                .deploy();

    }

    @Test
    public void testRunTimeProcess(){
        //使用classpath下的activiti.cfg.xml中的配置创建processEngine
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = defaultProcessEngine.getRuntimeService();
        ProcessInstance myEvection = runtimeService.startProcessInstanceByKey("sign");

        System.out.println("流程定义ID = " + myEvection.getProcessDefinitionId());
        System.out.println("流程实例ID = " + myEvection.getId());
        System.out.println("当前活动ID = " + myEvection.getActivityId());
    }

    @Test
    public void personalTaskList(){
        //使用classpath下的activiti.cfg.xml中的配置创建processEngine
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = defaultProcessEngine.getTaskService();
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("sign")
                .taskAssignee("王五").list();
//                .taskAssignee("rose").list();

        list.forEach(l->{
            System.out.println("流程实例ID = " + l.getProcessInstanceId());
            System.out.println("任务ID = " + l.getId());
            System.out.println("任务负责人 = " + l.getAssignee());
        });
    }

    // 完成个人任务
    private static void completeTask(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        RepositoryService repositoryService = processEngine.getRepositoryService();
        TaskService taskService = processEngine.getTaskService();

        taskService.complete("60002");
    }

    // 完成个人任务
    private static void deleteDe(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

        RepositoryService repositoryService = processEngine.getRepositoryService();
        repositoryService.deleteDeployment("77501",true);
    }

    public static void main(String[] args) {
//        completeTask();
        deleteDe();
    }
}
