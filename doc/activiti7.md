activiti7 是一个工作流引擎，使用专门的建模语言 `BPMN 2.0` 定义，业务系统安装制定的流程进行执行



每次获取流程引擎时会自动判断数据库中是否有 activiti7自带的25张表 如果没有会创建，有了则不再创建



获取流程引擎的两种方式

- 使用默认的方式获取流程引擎 但该方法有局限性 

  - 配置文件的名称必须为 activiti.cfg.xml

  - 配置文件中 配置的 流程引擎bean id必须为processEngineConfiguration

  - xml

    - ```
      <bean id="processEngineConfiguration" class="org.activiti.engine.impl.cfg.StandaloneProcessEngineConfiguration">
              <property name="jdbcDriver" value="com.mysql.jdbc.Driver"/>
              <property name="jdbcUrl" value="jdbc:mysql:///activiti?characterEncoding=UTF-8"/>
              <property name="jdbcUsername" value="root"/>
              <property name="jdbcPassword" value="root"/>
              <!-- activiti数据库表处理策略 -->
              <property name="databaseSchemaUpdate" value="true"/>
          </bean>
      ```

  - Java实现

    - ```java
       	@Test
          public void testCreateTable(){
              //使用classpath下的activiti.cfg.xml中的配置创建processEngine
              ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
              
              // 获取 RepositoryService
              RepositoryService repositoryService = defaultProcessEngine.getRepositoryService();
              
              // 如果表不存在则建表 存在则不建
              Deployment deployment = repositoryService.createDeployment()
      				// 流程定义的名称
                      .name("账号申请流程")
                  	// 流程文件以及流程图png的本地资源目录 资源会存在表（act_ge_bytearray）中 
                      .addClasspathResource("bpmn/sign.bpmn")
                      .addClasspathResource("bpmn/sign.png")
                      .deploy();
      
              System.out.println("defaultProcessEngine = " + defaultProcessEngine);
          }
      ```

  

- 可以指定配置文件的名称以及bean的id 来获取流程引擎

  - xml

  - java

    - ```java
      // 获取配置文件所在位置  
      InputStream inputStream =  this.getClass().getClassLoader().getResourceAsStream("activitiConfig.xml");
      
      // 该方法可以配置 两个参数 一个配置文件的流 一个bean的id 
              ProcessEngineConfiguration processEngineConfigurationConfiguration = ProcessEngineConfiguration.createProcessEngineConfigurationFromInputStream(inputStream, "processEngineConfigurationConfiguration");
      
              ProcessEngine processEngine = processEngineConfigurationConfiguration.buildProcessEngine();
              RepositoryService repositoryService1 = processEngine.getRepositoryService();
              Deployment deploy = repositoryService1.createDeployment()
                      .name("申请账号流程 自定义")
                      .addClasspathResource("bpmn/sign.bpmn")
                      .addClasspathResource("bpmn/sign.png")
                      .deploy();
      
      ```

    - 

  

